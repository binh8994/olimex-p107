	*** OLIMEX demo project for the STM32-P107 ***

1. Requirements
	- STM32-P107 demo board by Olimex
	- Compatible debugger, for example ARM-JTAG-EW
	- IAR EW v6.30.7 or later
	- RS232 port and a terminal program (Hyper Terminal, PuTTY, etc.)
	- miniUSB cable
	
2. Description
	The demo demonstrates the functionality of the USB OTG as a mouse device.
	
3. How to use this demo
	+ Open the workspace file: .\STM32-P107_DemoSoft_revC\STM32-P107_revC.eww
	+ There are 4 demos in this workspace. Select the one you need.
	+ Press F7 to compile the project.
	+ Connect the debugger to the PC and to the target board. Supply the board as needed.
	+ Press Ctrl+D to download the executable to the target.
	+ Connect the RS232 cable to the board and start your terminal program with the following settings: 115200-8N1
	+ Start debugging (F5)
	+ Follow the instructions on the terminal.
	
	Note: No drivers are needed, mouse should install automatically.
		
4. Support
	http://www.iar.com/
	http://www.olimex.com/dev/