/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   05/09/2016
 * @Update:
 * @AnhHH: Add io function for sth11 sensor.
 ******************************************************************************
**/
#ifndef __IO_CFG_H__
#define __IO_CFG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#include "misc.h"
#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "system_stm32f10x.h"
#include "core_cm3.h"
#include "core_cmFunc.h"

/******************************************************************************
* led status function
*******************************************************************************/
extern void led_life_init();
extern void led_life_on();
extern void led_life_off();

/******************************************************************************
* nfr24l01 IO function
*******************************************************************************/
extern void nrf24l01_io_ctrl_init();
extern void nrf24l01_ce_low();
extern void nrf24l01_ce_high();
extern void nrf24l01_csn_low();
extern void nrf24l01_csn_high();

/******************************************************************************
* ir IO function
*******************************************************************************/
extern void timer_10us_init();
extern void timer_10us_enable();
extern void timer_10us_disable();

extern void     ir_read_gpio_init();
extern uint32_t ir_read_gpio();

extern void ir_write_pwm_init();
extern void ir_write_pwm_on();
extern void ir_write_pwm_off();

/******************************************************************************
* shtx IO function
*******************************************************************************/
#define SHT1X_CLK_PIN           (0x01)
#define SHT1X_DATA_PIN          (0x02)

extern void sht1x_clk_input_mode();
extern void sht1x_clk_output_mode();
extern void sht1x_clk_digital_write_low();
extern void sht1x_clk_digital_write_high();

extern void sht1x_data_input_mode();
extern void sht1x_data_output_mode();
extern void sht1x_data_digital_write_low();
extern void sht1x_data_digital_write_high();
extern int  sht1x_data_digital_read();

#ifdef __cplusplus
}
#endif

#endif //__IO_CFG_H__
