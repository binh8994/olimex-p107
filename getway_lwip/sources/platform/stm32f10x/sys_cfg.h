/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   05/09/2016
 ******************************************************************************
**/
#ifndef __SYS_CFG_H__
#define __SYS_CFG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include "stm32f10x_conf.h"

/* USART Communication boards Interface */
//#define USARTx                           UART4
//#define USARTx_CLK                       RCC_APB1Periph_UART4
//#define USARTx_IRQn                      UART4_IRQn

//#define USARTx_TX_PIN                    GPIO_Pin_10
//#define USARTx_TX_GPIO_PORT              GPIOC
//#define USARTx_TX_GPIO_CLK               RCC_APB2Periph_GPIOC
//#define USARTx_TX_SOURCE                 GPIO_PinSource10

//#define USARTx_RX_PIN                    GPIO_Pin_11
//#define USARTx_RX_GPIO_PORT              GPIOC
//#define USARTx_RX_GPIO_CLK               RCC_APB2Periph_GPIOC
//#define USARTx_RX_SOURCE                 GPIO_PinSource11

#define USARTx                           USART3
#define USARTx_CLK                       RCC_APB1Periph_USART3
#define USARTx_IRQn                      USART3_IRQn

#define USARTx_TX_PIN					 GPIO_Pin_8
#define USARTx_TX_GPIO_PORT              GPIOD
#define USARTx_TX_GPIO_CLK               RCC_APB2Periph_GPIOD
#define USARTx_TX_SOURCE                 GPIO_PinSource8

#define USARTx_RX_PIN                    GPIO_Pin_9
#define USARTx_RX_GPIO_PORT              GPIOD
#define USARTx_RX_GPIO_CLK               RCC_APB2Periph_GPIOD
#define USARTx_RX_SOURCE                 GPIO_PinSource9

extern void sys_cfg_clock();
extern void sys_cfg_tick();
extern void sys_cfg_console();
extern void sys_cfg_update_info();

extern void sys_cfg_ethernet();

#ifdef __cplusplus
}
#endif

#endif //__SYS_CFG_H__
