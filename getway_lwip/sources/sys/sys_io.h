#ifndef __SYS_IO_H__
#define __SYS_IO_H__

#ifdef __cplusplus
extern "C"
{
#endif

#if defined(TIVA_PLATFORM)
#include "../platform/tiva/io_cfg.h"
#elif defined(STM32L_PLATFORM)
#include "../platform/stm32l/io_cfg.h"
#include "../platform/stm32l/arduino/Arduino.h"
#elif defined(STM32F10X_PLATFORM)
#include "../platform/stm32f10x/io_cfg.h"
#include "../platform/stm32f10x/arduino/Arduino.h"
#else
#error Please choose platform for app.
#endif

#ifdef __cplusplus
}
#endif

#endif // __SYS_IO_H__
