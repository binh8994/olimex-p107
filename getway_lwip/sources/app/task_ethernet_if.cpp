#include "task_ethernet_if.h"

#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"

#include "../sys/sys_dbg.h"

#include "lwip/memp.h"
#include "lwip/tcp.h"
#include "lwip/udp.h"
#include "netif/etharp.h"
#include "lwip/dhcp.h"
#include "ethernetif.h"

#include "app.h"
#include "task_list.h"
#include "app_dbg.h"

void task_ethernet_if(ak_msg_t* msg) {
    switch (msg->sig) {
    case ETHERNET_TCP_TMR:
        tcp_tmr();              /* called periodically every 250ms */
        break;

    case ETHERNET_ARP_TMR:
        etharp_tmr();           /* called periodically every 5s */
        break;

#if LWIP_DHCP
    case ETHERNET_DHCP_FINE_TMR:
        dhcp_fine_tmr();        /* called periodically every 500ms */
        break;

    case ETHERNET_DHCP_COURSE_TMR:
        dhcp_coarse_tmr();      /* called periodically every 60s */
        break;
#endif

    default:
        FATAL("TETH", 0x01);
        break;
    }
}
