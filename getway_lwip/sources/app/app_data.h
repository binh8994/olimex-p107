#ifndef __APP_DATA_H__
#define __APP_DATA_H__
#include <stdint.h>

/******************************************************************************
* Data type of RF24Network
*******************************************************************************/
#define ENVIROMENT_TYPE     (1)
#define REMOTE_IR_TYPE      (2)

/******************************************************************************
* Commom define for transceiver data
*******************************************************************************/
/* command IR */
#define REMOTE_POWER_ON     (0x01)
#define REMOTE_POWER_OFF    (0x02)


#endif //__APP_DATA_H__
