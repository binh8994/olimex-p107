#ifndef __TASK_LIST_H__
#define __TASK_LIST_H__

#include "../ak/ak.h"
#include "../ak/task.h"

extern task_t app_task_table[];

/*****************************************************************************/
/*  DECLARE: Task ID
 *  Note: Task id MUST be increasing order.
 */
/*****************************************************************************/
#define TASK_TIMER_TICK_ID              0
#define TASK_CONSOLE_ID                 1
#define TASK_LIFE_ID                    2
#define TASK_RF_IF_ID                   3
#define TASK_ETHERNET_IF_ID             4
#define TASK_SENSOR_ID                  5

/* EOT task ID */
#define TASK_EOT_ID                     6

/*****************************************************************************/
/*  DECLARE: Task entry point
 */
/*****************************************************************************/
extern void task_console(ak_msg_t*);
extern void task_life(ak_msg_t*);
extern void task_rf_if(ak_msg_t*);
extern void task_ethernet_if(ak_msg_t*);
extern void task_sensor(ak_msg_t*);

#endif //__TASK_LIST_H__
