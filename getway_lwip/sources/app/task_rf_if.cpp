#include <stdbool.h>
#include <stdint.h>

#include "app.h"
#include "app_data.h"
#include "app_dbg.h"

#include "task_rf_if.h"
#include "task_list.h"

#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"
#include "../ak/timer.h"

#include "../sys/sys_dbg.h"
#include "../sys/sys_irq.h"
#include "../sys/sys_io.h"

#include "../common/utils.h"

/**/
RF24 nrf24l01(1, 2);
RF24Network local_network(nrf24l01);
const uint16_t me_node_channel = 90;        /* node channel */
const uint16_t me_node_address = 00;        /* node address */

void sys_irq_nrf24l01() {
    bool tx_ok, tx_fail, rx_ready;
    nrf24l01.whatHappened(tx_ok, tx_fail, rx_ready);
    APP_DBG("sys_irq_nrf24l01\n");

    if (tx_ok) {
        ak_msg_t* msg = get_pure_msg();
        set_msg_sig(msg, NRF24L01_IRQ_TX_SUCCESS);
        task_post(TASK_RF_IF_ID, msg);
    }

    if (tx_fail) {
        ak_msg_t* msg = get_pure_msg();
        set_msg_sig(msg, NRF24L01_IRQ_TX_FAIL);
        task_post(TASK_RF_IF_ID, msg);
    }

    if (rx_ready) {
        ak_msg_t* msg = get_pure_msg();
        set_msg_sig(msg, NRF24L01_IRQ_RX_READY);
        task_post(TASK_RF_IF_ID, msg);
    }
}

void task_rf_if(ak_msg_t* msg) {
    switch (msg->sig) {
    case NRF24L01_IRQ_TX_FAIL:
        APP_DBG("\nNRF24L01_IRQ_TX_FAIL\n");
        FATAL("RF", 0x02);
        break;

    case NRF24L01_IRQ_TX_SUCCESS:
        APP_DBG("\nNRF24L01_IRQ_TX_SUCCESS\n");
        FATAL("RF", 0x03);
        break;

    case NRF24L01_IRQ_RX_READY:
        APP_DBG("\nNRF24L01_IRQ_RX_READY\n");
        local_network.update();
        if (local_network.available()) {
            RF24NetworkHeader rev_header;

            uint8_t rev_rf_data[32];
            mem_set(rev_rf_data, 0, 32);

            local_network.read(rev_header, rev_rf_data, 32);

            APP_DBG("from:%d\tto:%d\tid:%d\ttype:%d\n", \
                    rev_header.from_node, \
                    rev_header.to_node, \
                    rev_header.id, \
                    rev_header.type);

            if (rev_header.type == ENVIROMENT_TYPE) {
                ak_msg_t* msg = get_common_msg();
                set_msg_sig(msg, SENSOR_LOCAL_REPORT);
                set_data_common_msg(msg, rev_rf_data, sizeof(sensor_packet_t));
                task_post(TASK_SENSOR_ID, msg);
            }
            else {
                FATAL("RF", 0x02);
            }
        }
        break;

    case RF_IF_INIT_NETWORK:
        APP_DBG("\nRF_IF_INIT_NETWORK\n");
        nrf24l01.begin();
        nrf24l01.maskIRQ(0, 0, 1);  /* enable rx interrupt */
        local_network.begin(me_node_channel, me_node_address);
        nrf24l01.printDetails();
        break;

    case RF_IF_CTL_IR_POWER: {
        RF24NetworkHeader end_device_header((uint16_t)01, REMOTE_IR_TYPE);
        bool send_success = local_network.write(end_device_header, get_data_common_msg(msg), sizeof(uint8_t));
        if (send_success) {
            APP_DBG("send success.\n");
        }        else {
            APP_DBG("send fault.\n");
        }
    }
        break;

    default:
        FATAL("RF", 0x01);
        break;
    }
}
