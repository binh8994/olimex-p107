#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"

#include "../driver/led/led.h"

#include "app.h"
#include "task_list.h"
#include "app_dbg.h"

void task_life(ak_msg_t* msg) {
    switch (msg->sig) {
    case LIFE_SYSTEM_CHECK:
        led_toggle(&led_life);
        break;

    default:
        break;
    }
}
