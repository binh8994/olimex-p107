/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

/* kernel include */
#include "../ak/ak.h"
#include "../ak/message.h"
#include "../ak/timer.h"

/* driver include */
#include "../driver/led/led.h"

/* app include */
#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_console.h"

/* sys include */
#include "../sys/sys_irq.h"
#include "../sys/sys_io.h"
#include "../sys/sys_ctrl.h"

/* app variable define */
led_t led_life;

int app_main() {
    APP_PRINT("app_main() entry OK\n");

    /* init active kernel */
    ENTRY_CRITICAL();
    task_init();
    task_create(app_task_table);
    EXIT_CRITICAL();

    /* init application */
    led_init(&led_life, led_life_init, led_life_on, led_life_off);
    app_start_timer();

    /* run task */
    return task_run();
}

void app_start_timer() {
    /* start timer to toggle life led */
    timer_set(TASK_LIFE_ID, LIFE_SYSTEM_CHECK, LIFE_TASK_TIMER_LED_LIFE_INTERVAL, TIMER_PERIODIC);

    /* start timer to start up RF */
    timer_set(TASK_RF_IF_ID, RF_IF_INIT_NETWORK, RF_IF_TIMER_INTERVAL_INIT_NETWORK, TIMER_ONE_SHOT);

    /* start timer for TCP periodic process every 250 ms */
    timer_set(TASK_ETHERNET_IF_ID, ETHERNET_TCP_TMR, ETHERNET_IF_TCP_TMR_INTERVAL, TIMER_PERIODIC);

    /* start timer for ARP periodic process every 5s */
    timer_set(TASK_ETHERNET_IF_ID, ETHERNET_ARP_TMR, ETHERNET_IF_ARP_TMR_INTERVAL, TIMER_PERIODIC);

#if LWIP_DHCP
  /* start timer Fine DHCP periodic process every 500ms */
    timer_set(TASK_ETHERNET_IF_ID, ETHERNET_DHCP_FINE_TMR, ETHERNET_IF_DHCP_FINE_TMR_INTERVAL, TIMER_PERIODIC);

  /* start timer DHCP Coarse periodic process every 60s */
    timer_set(TASK_ETHERNET_IF_ID, ETHERNET_DHCP_COURSE_TMR, ETHERNET_IF_DHCP_COURSE_TMR_INTERVAL, TIMER_PERIODIC);
#endif
}

void sys_irq_timer_10us() {

}
