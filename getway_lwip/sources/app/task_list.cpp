#include "task_list.h"
#include "../ak/timer.h"

task_t app_task_table[] = {
    /*************************************************************************/
    /* SYSTEM TASK */
    /*************************************************************************/
    {TASK_TIMER_TICK_ID,    TASK_PRI_LEVEL_7,       task_timer_tick         },

    /*************************************************************************/
    /* APP TASK */
    /*************************************************************************/
    {TASK_CONSOLE_ID,       TASK_PRI_LEVEL_1,       task_console            },
    {TASK_LIFE_ID,          TASK_PRI_LEVEL_1,       task_life               },
    {TASK_RF_IF_ID,         TASK_PRI_LEVEL_5,       task_rf_if              },
    {TASK_ETHERNET_IF_ID,   TASK_PRI_LEVEL_6,       task_ethernet_if        },
    {TASK_SENSOR_ID,        TASK_PRI_LEVEL_1,       task_sensor             },

    /*************************************************************************/
    /* END OF TABLE */
    /*************************************************************************/
    {TASK_EOT_ID, 0,    (pf_task)0}
};

