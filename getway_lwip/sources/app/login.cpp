/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

#include <stdint.h>
#include <math.h>

#include "../ak/ak.h"
#include "../ak/task.h"
#include "../ak/timer.h"

#include "../common/cmd_line.h"
#include "../common/utils.h"
#include "../common/xprintf.h"

#include "../sys/sys_ctrl.h"
#include "../sys/sys_io.h"

#include "task_console.h"
#include "task_list.h"
#include "task_rf_if.h"

#include "app.h"
#include "app_dbg.h"
#include "app_data.h"

/*****************************************************************************/
/*  command function declare
 */
/*****************************************************************************/
static int32_t i_lgn_ver(uint8_t* argv);
static int32_t i_lgn_rst(uint8_t* argv);
static int32_t i_lgn_rmt(uint8_t* argv);

/*****************************************************************************/
/*  command table
 */
/*****************************************************************************/
cmd_line_t gst_cmdTable[] = {
    /*************************************************************************/
    /* system command */
    /*************************************************************************/
    {(const int8_t*)"ver", i_lgn_ver, (const int8_t*)"get version"},
    {(const int8_t*)"rst", i_lgn_rst, (const int8_t*)"soft reset"},

    /*************************************************************************/
    /* debug command */
    /*************************************************************************/
    {(const int8_t*)"rmt", i_lgn_rmt, (const int8_t*)""},

    /* End Of Table */
    {(const int8_t*)0,(pf_cmd_func)0,(const int8_t*)0}
};

/*****************************************************************************/
/*  command function definaion
 */
/*****************************************************************************/
int32_t i_lgn_ver(uint8_t* argv) {
    (void)argv;
    APP_DBG("kernel version:%s\n", AK_VERSION);
    return 0;
}

int32_t i_lgn_rst(uint8_t* argv) {
    (void)argv;
    sys_ctrl_reset();
    return 0;
}

int32_t i_lgn_rmt(uint8_t* argv) {
    (void)argv;
    uint8_t ir_data = 0;
    ak_msg_t* msg = get_common_msg();
    set_msg_sig(msg, SENSOR_CTL_IR_POWER);

    if (*(argv + 4) == 'o') {
        ir_data = REMOTE_POWER_ON;
    }
    else {
        ir_data = REMOTE_POWER_OFF;
    }
    set_data_common_msg(msg, &ir_data, sizeof(uint8_t));

    task_post(TASK_SENSOR_ID, msg);
    return 0;
}
