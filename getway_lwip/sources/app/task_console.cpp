/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

#include <stdbool.h>

#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"

#include "../common/cmd_line.h"
#include "../common/utils.h"

#include "../sys/sys_ctrl.h"
#include "../sys/sys_irq.h"

#include "app.h"
#include "app_dbg.h"
#include "task_console.h"
#include "task_list.h"

struct console_t {
    uint8_t index;
    uint8_t data[CONSOLE_BUFFER_LENGHT];
} console;

void sys_irq_console() {
    uint8_t c = 0;
    c = sys_ctrl_console_get_char();
    if (console.index < CONSOLE_BUFFER_LENGHT - 1) {
        if (c == '\r' || c == '\n') {
            console.data[console.index] = c;
            console.data[console.index + 1] = 0;

            {
                ak_msg_t* msg = get_common_msg();
                set_msg_sig(msg, CONSOLE_CMD);
                set_data_common_msg(msg, &console.data[0], console.index + 2);
                task_post(TASK_CONSOLE_ID, msg);
            }

            console.index = 0;
        }
        else {
            console.data[console.index++] = c;
        }
    }
    else {
        APP_PRINT("\nerror: cmd too long, cmd size: %d, try again !\n", CONSOLE_BUFFER_LENGHT);
        console.index = 0;
    }
}

void task_console(ak_msg_t* msg) {
    uint8_t msg_data[COMMON_MSG_DATA_SIZE];

    mem_cpy(msg_data, get_data_common_msg(msg), COMMON_MSG_DATA_SIZE);

    switch (msg->sig) {
    case CONSOLE_CMD:
        switch (cmd_line_parser(msg_data)) {
        case CMD_SUCCESS:
            break;

        case CMD_NOT_FOUND:
            if (msg_data[0] != '\r' &&
                    msg_data[0] != '\n') {
                APP_PRINT("error: command unknown\n");
            }
            break;

        case CMD_TOO_LONG:
            APP_PRINT("error: command too long\n");
            break;

        default:
            APP_PRINT("error: command error\n");
            break;
        }

        APP_PRINT("#");
        break;

    default:
        break;
    }
}
