/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   05/09/2016
 * @Update:
 * @AnhHH: Add io function for sth11 sensor.
 ******************************************************************************
**/
#include <stdint.h>
#include <stdbool.h>

#include "io_cfg.h"
#include "stm32.h"

/******************************************************************************
* led status function
*******************************************************************************/
void led_life_init() {
    GPIO_InitTypeDef        GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
}

void led_life_on() {
    GPIO_SetBits(GPIOB, GPIO_Pin_0);
}

void led_life_off() {
    GPIO_ResetBits(GPIOB, GPIO_Pin_0);
}

/******************************************************************************
* nfr24l01 IO function
*******************************************************************************/
void nrf24l01_io_ctrl_init() {
    /* CE / CSN / IRQ */
    GPIO_InitTypeDef        GPIO_InitStructure;
    EXTI_InitTypeDef        EXTI_InitStruct;
    NVIC_InitTypeDef        NVIC_InitStruct;

    /* GPIOC Periph clock enable */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

    /* IRQ -> PA4 */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource4);

    EXTI_InitStruct.EXTI_Line = EXTI_Line4;
    EXTI_InitStruct.EXTI_LineCmd = ENABLE;
    EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_Init(&EXTI_InitStruct);

    NVIC_InitStruct.NVIC_IRQChannel = EXTI4_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStruct);
}

void nrf24l01_ce_low() {
    GPIO_ResetBits(GPIOC, GPIO_Pin_4);
}

void nrf24l01_ce_high() {
    GPIO_SetBits(GPIOC, GPIO_Pin_4);
}

void nrf24l01_csn_low() {
    GPIO_ResetBits(GPIOC, GPIO_Pin_5);
}

void nrf24l01_csn_high() {
    GPIO_SetBits(GPIOC, GPIO_Pin_5);
}

/******************************************************************************
* ir IO function
*******************************************************************************/
void timer_10us_init() {
    TIM_TimeBaseInitTypeDef  timer_10us;
    NVIC_InitTypeDef NVIC_InitStructure;

    /* timer 10us to polling receive IR signal */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);
    timer_10us.TIM_Period = 319;
    timer_10us.TIM_Prescaler = 0;
    timer_10us.TIM_ClockDivision = 0;
    timer_10us.TIM_CounterMode = TIM_CounterMode_Down;
    TIM_TimeBaseInit(TIM6, &timer_10us);

    NVIC_InitStructure.NVIC_IRQChannel = TIM6_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    TIM_ITConfig(TIM6, TIM_IT_Update, ENABLE);
    TIM_Cmd(TIM6, ENABLE);

    /* PWM to create IR output frequency */
}

void timer_10us_enable() {
    ENTRY_CRITICAL();
    TIM_Cmd(TIM6, ENABLE);
    EXIT_CRITICAL();
}

void timer_10us_disable() {
    ENTRY_CRITICAL();
    TIM_Cmd(TIM6, DISABLE);
    EXIT_CRITICAL();
}

void ir_read_gpio_init() {
    GPIO_InitTypeDef        GPIO_InitStructure;
    EXTI_InitTypeDef        EXTI_InitStruct;
    NVIC_InitTypeDef        NVIC_InitStruct;

    /* ir read -> PB1*/
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource1);

    EXTI_InitStruct.EXTI_Line = EXTI_Line1;
    EXTI_InitStruct.EXTI_LineCmd = ENABLE;
    EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_Init(&EXTI_InitStruct);

    NVIC_InitStruct.NVIC_IRQChannel = EXTI1_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStruct);
}

uint32_t ir_read_gpio() {
    return (uint32_t)GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_1);
}

void ir_write_pwm_init() {
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    GPIO_InitTypeDef GPIO_InitStructure;
    TIM_OCInitTypeDef  TIM_OCInitStructure;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    GPIO_PinRemapConfig(GPIO_FullRemap_TIM2, ENABLE);

    TIM_TimeBaseStructure.TIM_Period = 841;     /* 32 Mhz / 842 = 38Khz */
    TIM_TimeBaseStructure.TIM_Prescaler = 0;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = 281;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

    TIM_OC3Init(TIM2, &TIM_OCInitStructure);

    TIM_OC1PreloadConfig(TIM2, TIM_OCPreload_Enable);

    TIM_ARRPreloadConfig(TIM2, ENABLE);

    TIM_Cmd(TIM2, ENABLE);
}

void ir_write_pwm_on() {
    ENTRY_CRITICAL();
    TIM_Cmd(TIM2, ENABLE);
    EXIT_CRITICAL();
}

void ir_write_pwm_off() {
    ENTRY_CRITICAL();
    TIM_Cmd(TIM2, DISABLE);
    EXIT_CRITICAL();
}

/******************************************************************************
* sht11 IO function
*******************************************************************************/
void sht1x_clk_input_mode(){

}

void sht1x_clk_output_mode(){

}

void sht1x_clk_digital_write_low() {

}

void sht1x_clk_digital_write_high() {

}

void sht1x_data_input_mode() {

}

void sht1x_data_output_mode() {

}

void sht1x_data_digital_write_low() {

}

void sht1x_data_digital_write_high() {

}

int sht1x_data_digital_read() {
    return 0;
}

