# header path
CPPFLAGS	+= -I./sources/platform/stm32f10x/arduino/SPI
CPPFLAGS	+= -I./sources/platform/stm32f10x/Libraries/STM32F10x_StdPeriph_Driver/inc
CPPFLAGS	+= -I./sources/platform/stm32f10x/Libraries/CMSIS/Device/ST/STM32F10x
CPPFLAGS	+= -I./sources/platform/stm32f10x/Libraries/CMSIS/Include
CPPFLAGS	+= -I./sources/platform/stm32f10x

# source path
VPATH += sources/platform/stm32f10x/arduino
VPATH += sources/platform/stm32f10x/arduino/SPI

# CPP source files
SOURCES_CPP += sources/platform/stm32f10x/arduino/SPI/SPI.cpp
SOURCES_CPP += sources/platform/stm32f10x/arduino/wiring_digital.cpp
SOURCES_CPP += sources/platform/stm32f10x/arduino/wiring_shift.cpp
