CFLAGS += -I./sources/platform/stm32f10x/Libraries/CMSIS/Device/ST/STM32F10x
CFLAGS += -I./sources/platform/stm32f10x/Libraries/CMSIS/Include

VPATH += sources/platform/stm32f10x/Libraries/CMSIS/Device/ST/STM32F10x

# C source files
SOURCES += sources/platform/stm32f10x/Libraries/CMSIS/Device/ST/STM32F10x/system_stm32f10x.c
