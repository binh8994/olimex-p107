CFLAGS += -I./sources/platform/stm32f10x/Libraries/STM32F10x_StdPeriph_Driver/inc
CFLAGS += -I./sources/platform/stm32f10x

VPATH += sources/platform/stm32f10x/Libraries/STM32F10x_StdPeriph_Driver/src
VPATH += sources/platform/stm32f10x

# C source files
SOURCES += ./sources/platform/stm32f10x/Libraries/STM32F10x_StdPeriph_Driver/src/stm32f10x_gpio.c
SOURCES += ./sources/platform/stm32f10x/Libraries/STM32F10x_StdPeriph_Driver/src/stm32f10x_rcc.c
SOURCES += ./sources/platform/stm32f10x/Libraries/STM32F10x_StdPeriph_Driver/src/stm32f10x_usart.c
SOURCES += ./sources/platform/stm32f10x/Libraries/STM32F10x_StdPeriph_Driver/src/stm32f10x_spi.c
SOURCES += ./sources/platform/stm32f10x/Libraries/STM32F10x_StdPeriph_Driver/src/misc.c
SOURCES += ./sources/platform/stm32f10x/Libraries/STM32F10x_StdPeriph_Driver/src/stm32f10x_exti.c
SOURCES += ./sources/platform/stm32f10x/Libraries/STM32F10x_StdPeriph_Driver/src/stm32f10x_tim.c
