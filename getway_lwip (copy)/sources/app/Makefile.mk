CPPFLAGS	+= -I./sources/network/lwip-1.3.1/port
CPPFLAGS	+= -I./sources/network/lwip-1.3.1/src/include
CPPFLAGS	+= -I./sources/network/lwip-1.3.1/src/include/ipv4
CPPFLAGS	+= -I./sources/app

VPATH += sources/app

# CPP source files
SOURCES_CPP += sources/app/app.cpp
SOURCES_CPP += sources/app/task_console.cpp
SOURCES_CPP += sources/app/task_life.cpp
SOURCES_CPP += sources/app/login.cpp
SOURCES_CPP += sources/app/task_list.cpp
SOURCES_CPP += sources/app/task_rf_if.cpp
SOURCES_CPP += sources/app/task_ethernet_if.cpp
SOURCES_CPP += sources/app/task_sensor.cpp
