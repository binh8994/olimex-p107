#ifndef __TASK_RF_IF_H__
#define __TASK_RF_IF_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "../rf_protocols/RF24/RF24.h"
#include "../rf_protocols/RF24Network/RF24Network.h"

extern RF24 nrf24l01;
extern RF24Network local_network;

#ifdef __cplusplus
}
#endif

#endif // __TASK_RF_IF_H__
