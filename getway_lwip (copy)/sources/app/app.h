/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

#ifndef APP_H
#define APP_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "../driver/led/led.h"
#include "lwipopts.h"

/*****************************************************************************/
/*  life task define
 */
/*****************************************************************************/
/* define timer */
#define LIFE_TASK_TIMER_LED_LIFE_INTERVAL       (1000)

/* define signal */
#define LIFE_SYSTEM_CHECK                       (0)


/*****************************************************************************/
/*  console task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
#define CONSOLE_CMD                             (0)


/*****************************************************************************/
/*  rf if task define
 */
/*****************************************************************************/
/* define timer */
/* delay time to start RF */
#define RF_IF_TIMER_INTERVAL_INIT_NETWORK       (100)   /* 100ms */

/* define signal */
/* interrupt signal */
#define NRF24L01_IRQ_TX_FAIL                    (1)
#define NRF24L01_IRQ_TX_SUCCESS                 (2)
#define NRF24L01_IRQ_RX_READY                   (3)

/* task signal */
#define RF_IF_INIT_NETWORK                      (4)
#define RF_IF_CTL_IR_POWER                      (5)

/*****************************************************************************/
/*  ethernet task define
 */
/*****************************************************************************/
/* define timer */
#define ETHERNET_IF_TCP_TMR_INTERVAL            (250)   /* 250 ms*/
#define ETHERNET_IF_ARP_TMR_INTERVAL            (5000)  /* 5s */

#if LWIP_DHCP
#define ETHERNET_IF_DHCP_FINE_TMR_INTERVAL      (500)   /* 500 ms*/
#define ETHERNET_IF_DHCP_COURSE_TMR_INTERVAL    (60000) /* 60s */
#endif

/* define signal */
#define ETHERNET_TCP_TMR                        (1)
#define ETHERNET_ARP_TMR                        (2)
#if LWIP_DHCP
#define ETHERNET_DHCP_FINE_TMR                  (3)
#define ETHERNET_DHCP_COURSE_TMR                (4)
#endif

/*****************************************************************************/
/*  sensor task define
 */
/*****************************************************************************/
/* define timer */

/* define signal */
#define SENSOR_LOCAL_REPORT                     (1)
#define SENSOR_CTL_IR_POWER                     (2)

/* define variable */
typedef struct {
    uint8_t local_temperature;
    uint8_t local_humidity;
    uint8_t remote_temperature;
} sensor_packet_t;

/*****************************************************************************/
/*  global define variable
 */
/*****************************************************************************/
extern led_t led_life;

/*****************************************************************************/
/*  app function declare
 */
/*****************************************************************************/
extern void app_start_timer();
extern void app_interrupt_timer_10ms();
extern int  app_main();

#ifdef __cplusplus
}
#endif

#endif //APP_H
