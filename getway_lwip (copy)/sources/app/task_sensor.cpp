#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"

#include "../sys/sys_dbg.h"

#include "app.h"
#include "task_sensor.h"
#include "task_list.h"
#include "app_dbg.h"

void task_sensor(ak_msg_t* msg) {
    switch (msg->sig) {
    case SENSOR_LOCAL_REPORT: {
        uint8_t* sensor_data = get_data_common_msg(msg);

        APP_DBG("l_temp:%d\tl_hum:%d\tr_temp:%d\n", ((sensor_packet_t*)sensor_data)->local_temperature, \
                ((sensor_packet_t*)sensor_data)->local_humidity, \
                ((sensor_packet_t*)sensor_data)->remote_temperature);
    }
        break;

    case SENSOR_CTL_IR_POWER: {
        ak_msg_t* ir_power_msg = get_common_msg();
        set_msg_sig(ir_power_msg, RF_IF_CTL_IR_POWER);
        set_data_common_msg(ir_power_msg, get_data_common_msg(msg), sizeof(uint8_t));
        task_post(TASK_RF_IF_ID, ir_power_msg);
    }
        break;

    default:
        FATAL("SESR", 0x01);
        break;
    }
}
