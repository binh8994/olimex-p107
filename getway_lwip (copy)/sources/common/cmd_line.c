/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

#include "cmd_line.h"
#include "../common/utils.h"
#include "../common/xprintf.h"

uint8_t cmd_line_parser(uint8_t* command){
    uint8_t     cmd[MAX_CMD_SIZE];
    uint8_t*    p_command = command;
    uint8_t     cmd_index = 0;
    uint8_t     indexCheck = 0;

    /* get cmd */
    while(*p_command) {
        if (*p_command == ' ' || *p_command == '\r' || *p_command == '\n') {
            cmd[cmd_index] = 0;
            break;
        }
        else {
            cmd[cmd_index++] = *(p_command++);
            if (cmd_index >= MAX_CMD_SIZE) {
                return CMD_TOO_LONG;
            }
        }
    }

    /* find respective command in command table */
    while(gst_cmdTable[indexCheck].cmd){

        if(str_cmp(gst_cmdTable[indexCheck].cmd, (const int8_t*)cmd) == 0){

            /* perform respective function */
            gst_cmdTable[indexCheck].func(command);

            /* return success */
            return CMD_SUCCESS;
        }

        indexCheck++;
    }

    return CMD_NOT_FOUND;  /* command not found */
}

