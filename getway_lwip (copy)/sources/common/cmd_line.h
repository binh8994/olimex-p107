/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

#ifndef __cmd_line_t_H__
#define __cmd_line_t_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#define MAX_CMD_SIZE            12

#define CMD_SUCCESS             0
#define CMD_NOT_FOUND           1
#define CMD_TOO_LONG            2

typedef int32_t (*pf_cmd_func)(uint8_t* argv);

typedef struct {
    const int8_t* cmd;
    pf_cmd_func func;
    const int8_t* info;
}cmd_line_t;

/* this table have to be define in main file */
extern cmd_line_t gst_cmdTable[];

extern uint8_t cmd_line_parser(uint8_t* command);

#ifdef __cplusplus
}
#endif

#endif //__cmd_line_t_H__
