/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/
#ifndef __XPRINTF_H__
#define __XPRINTF_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdarg.h>
#include <stdint.h>

extern void (*xfunc_out)(uint8_t);
extern void xprintf(const int8_t* fmt, ...);
extern int xsprintf(char * str,const char* format, ...);

#ifdef __cplusplus
}
#endif

#endif //__XPRINTF_H__
