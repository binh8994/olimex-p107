CFLAGS	+= -I./sources/network/lwip-1.3.1/port

CFLAGS	+= -I./sources/network/lwip-1.3.1/src/include/ipv4
CFLAGS	+= -I./sources/network/lwip-1.3.1/src/include
CFLAGS	+= -I./sources/network/lwip-1.3.1/src/include/lwip

VPATH += sources/network/lwip-1.3.1/port
VPATH += sources/network/lwip-1.3.1/src/api
VPATH += sources/network/lwip-1.3.1/src/core
VPATH += sources/network/lwip-1.3.1/src/netif
VPATH += sources/network/lwip-1.3.1/src/core/ipv4

SOURCES += sources/network/lwip-1.3.1/src/api/api_lib.c
SOURCES += sources/network/lwip-1.3.1/src/api/api_msg.c
SOURCES += sources/network/lwip-1.3.1/src/api/err.c
SOURCES += sources/network/lwip-1.3.1/src/api/netbuf.c
SOURCES += sources/network/lwip-1.3.1/src/api/netdb.c
SOURCES += sources/network/lwip-1.3.1/src/api/netifapi.c
SOURCES += sources/network/lwip-1.3.1/src/api/sockets.c
SOURCES += sources/network/lwip-1.3.1/src/api/tcpip.c

SOURCES += sources/network/lwip-1.3.1/src/core/dhcp.c
SOURCES += sources/network/lwip-1.3.1/src/core/dns.c
SOURCES += sources/network/lwip-1.3.1/src/core/init.c
SOURCES += sources/network/lwip-1.3.1/src/core/mem.c
SOURCES += sources/network/lwip-1.3.1/src/core/memp.c
SOURCES += sources/network/lwip-1.3.1/src/core/netif.c
SOURCES += sources/network/lwip-1.3.1/src/core/pbuf.c
SOURCES += sources/network/lwip-1.3.1/src/core/raw.c
SOURCES += sources/network/lwip-1.3.1/src/core/stats.c
SOURCES += sources/network/lwip-1.3.1/src/core/sys.c
SOURCES += sources/network/lwip-1.3.1/src/core/tcp.c
SOURCES += sources/network/lwip-1.3.1/src/core/tcp_in.c
SOURCES += sources/network/lwip-1.3.1/src/core/tcp_out.c
SOURCES += sources/network/lwip-1.3.1/src/core/udp.c

SOURCES += sources/network/lwip-1.3.1/src/core/ipv4/autoip.c
SOURCES += sources/network/lwip-1.3.1/src/core/ipv4/icmp.c
SOURCES += sources/network/lwip-1.3.1/src/core/ipv4/igmp.c
SOURCES += sources/network/lwip-1.3.1/src/core/ipv4/inet.c
SOURCES += sources/network/lwip-1.3.1/src/core/ipv4/inet_chksum.c
SOURCES += sources/network/lwip-1.3.1/src/core/ipv4/ip_addr.c
SOURCES += sources/network/lwip-1.3.1/src/core/ipv4/ip.c
SOURCES += sources/network/lwip-1.3.1/src/core/ipv4/ip_frag.c

SOURCES += sources/network/lwip-1.3.1/src/netif/etharp.c
SOURCES += sources/network/lwip-1.3.1/src/netif/ethernetif.c
SOURCES += sources/network/lwip-1.3.1/src/netif/loopif.c
SOURCES += sources/network/lwip-1.3.1/src/netif/slipif.c

SOURCES += sources/network/lwip-1.3.1/port/ethernetif.c

