/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

#ifndef __MESSAGE_H__
#define __MESSAGE_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include "ak.h"

#define AK_MSG_NULL ((ak_msg_t*)0)

#define AK_MSG_NG                   (0)
#define AK_MSG_OK                   (1)

#define AK_MSG_TYPE_MASK            (0xC0)
#define AK_MSG_REF_COUNT_MASK       (0x3F)

#define AK_MSG_REF_COUNT_MAX        (7)

#define get_msg_ref_count(x)        ((((ak_msg_t*)x)->ref_count) & AK_MSG_REF_COUNT_MASK)
#define get_msg_type(x)             ((((ak_msg_t*)x)->ref_count) & AK_MSG_TYPE_MASK)

typedef struct {
    /* time of message handler */
    uint32_t    dbg_exe_msg_time;
} dbg_handler_t;

typedef struct ak_msg_t {
    /*******************************
     * private for kernel.
     ******************************/
    /* manage event */
    struct ak_msg_t*    next;

    /* task header */
    uint8_t             task_id;
    uint8_t             ref_count;
    uint8_t             sig;

    /*******************************
     * kernel debug.
     ******************************/
    /* task debug */
#if (AK_TASK_DEBUG == AK_ENABLE)
    dbg_handler_t      dbg_handler;
#endif

    /*******************************
     * public for user application.
     ******************************/

} ak_msg_t;

#define set_msg_sig(m, s) (((ak_msg_t*)m)->sig = s)

extern void         msg_init();
extern void         msg_free(ak_msg_t* msg);
extern void         msg_inc_ref_count(ak_msg_t* msg);
extern void         msg_dec_ref_count(ak_msg_t* msg);


/*****************************************************************************
 * DEFINITION: common message
 *
 *****************************************************************************/
#define COMMON_MSG_TYPE               (0xC0)
#define PURE_MSG_TYPE                 (0x80)
#define USER_MSG_TYPE                 (0x40)

extern ak_msg_t*    get_common_msg();
extern uint8_t      set_data_common_msg(ak_msg_t* msg, uint8_t* data, uint8_t size);
extern uint8_t*     get_data_common_msg(ak_msg_t* msg);

extern ak_msg_t*    get_pure_msg();

#ifdef __cplusplus
}
#endif

#endif //__MESSAGE_H__
