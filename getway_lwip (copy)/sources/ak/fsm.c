/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

#include "fsm.h"

void fsm_dispatch(fsm* me, ak_msg_t* msg) {
	me->state(msg);
}
