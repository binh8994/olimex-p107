/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   12/09/2016
 ******************************************************************************
**/
#ifndef __SYS_CTRL_H__
#define __SYS_CTRL_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

/* reset system (soft reset) */
extern void sys_ctrl_reset();

/* delay 3 cycles clock of system */
extern void sys_ctrl_delay(uint32_t count);

/* system delay ms unit, this function using timer delay */
extern void sys_ctrl_delay_ms(uint32_t count);

/* system delay us, uint this function using CPU delay*/
extern void sys_ctrl_delay_us(uint32_t count);

/* get current system timer variable */
extern uint32_t sys_ctrl_millis();

/* get character of system console */
extern uint8_t sys_ctrl_console_get_char();

#ifdef __cplusplus
}
#endif

#endif // __SYS_CTRL_H__
