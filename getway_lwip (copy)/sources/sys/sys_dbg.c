/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   05/09/2016
 ******************************************************************************
**/
#include "sys_dbg.h"
#include "sys_ctrl.h"
#include "../ak/ak.h"

#if defined(STM32L_PLATFORM)
#include "../common/xprintf.h"
#include "../platform/stm32l/io_cfg.h"
#include "../platform/stm32l/sys_cfg.h"
#elif defined(STM32F10X_PLATFORM)
#include "../common/xprintf.h"
#include "../platform/stm32f10x/io_cfg.h"
#include "../platform/stm32f10x/sys_cfg.h"
#else
#error Please choose platform for kernel.
#endif


void sys_dbg_fatal(const int8_t* s, uint8_t c) {
    unsigned char rev_c = 0;

#if defined(TIVA_PLATFORM)
    UARTprintf((const char*)"%s\t%x\n", s, c);
#endif
#if defined(STM32L_PLATFORM) || defined(STM32F10X_PLATFORM)
    xprintf((const int8_t *)"%s\t%x\n", s, c);
#endif

    /* Disable all interrupt */
    DISABLE_INTERRUPTS();

    while(1) {
        /* FATAL debug option */
        rev_c = sys_ctrl_console_get_char();
        if (rev_c) {
            switch (rev_c) {
            case 'r':
                sys_ctrl_reset();
                break;

            default:
                break;
            }
        }

        /* led notify FATAL */
        led_life_on();
        sys_ctrl_delay_us(300000);
        led_life_off();
        sys_ctrl_delay_us(300000);
    }
}

