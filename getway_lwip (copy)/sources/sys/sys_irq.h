#ifndef __SYS_IRQ_H__
#define __SYS_IRQ_H__

#ifdef __cplusplus
extern "C"
{
#endif

extern void sys_irq_nrf24l01();
extern void sys_irq_timer_10us();
extern void sys_irq_console();

#ifdef __cplusplus
}
#endif

#endif // __SYS_IRQ_H__
