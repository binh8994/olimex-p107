CFLAGS		+= -I./sources/driver/led
CPPFLAGS	+= -I./sources/driver/ir
CPPFLAGS	+= -I./sources/driver/SHT1x

VPATH += sources/driver/led
VPATH += sources/driver/ir
VPATH += sources/driver/SHT1x


SOURCES += sources/driver/led/led.c

SOURCES_CPP += sources/driver/ir/ir.cpp
SOURCES_CPP += sources/driver/SHT1x/SHT1x.cpp

