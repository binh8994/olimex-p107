CFLAGS	+= -I./sources/network/lwip-1.3.1/port
CFLAGS	+= -I./sources/network/lwip-1.3.1/port/arch
#CFLAGS	+= -I./sources/network/lwip-1.3.1/port/arch/junk

CFLAGS	+= -I./sources/network/lwip-1.3.1/src/include
CFLAGS	+= -I./sources/network/lwip-1.3.1/src/include/ipv4
CFLAGS	+= -I./sources/network/lwip-1.3.1/src/include/netif
#CFLAGS	+= -I./sources/network/lwip-1.3.1/src/netif/ppp

CPPFLAGS += -I./sources/network/lwip-1.3.1/port
CPPFLAGS += -I./sources/network/lwip-1.3.1/port/arch
#CPPFLAGS += -I./sources/network/lwip-1.3.1/port/arch/junk

CPPFLAGS += -I./sources/network/lwip-1.3.1/src/include
CPPFLAGS += -I./sources/network/lwip-1.3.1/src/include/ipv4
CPPFLAGS += -I./sources/network/lwip-1.3.1/src/include/netif
#CPPFLAGS = -I./sources/network/lwip-1.3.1/src/netif/ppp

VPATH += sources/network/lwip-1.3.1/port
#VPATH += sources/network/lwip-1.3.1/port/junk
VPATH += sources/network/lwip-1.3.1/src/api
VPATH += sources/network/lwip-1.3.1/src/core
VPATH += sources/network/lwip-1.3.1/src/netif
VPATH += sources/network/lwip-1.3.1/src/core/ipv4
VPATH += sources/network/lwip-1.3.1/src/core/snmp

SOURCES += sources/network/lwip-1.3.1/src/core/dhcp.c
SOURCES += sources/network/lwip-1.3.1/src/core/dns.c
SOURCES += sources/network/lwip-1.3.1/src/core/init.c
SOURCES += sources/network/lwip-1.3.1/src/core/mem.c
SOURCES += sources/network/lwip-1.3.1/src/core/memp.c
SOURCES += sources/network/lwip-1.3.1/src/core/netif.c
SOURCES += sources/network/lwip-1.3.1/src/core/pbuf.c
SOURCES += sources/network/lwip-1.3.1/src/core/raw.c
SOURCES += sources/network/lwip-1.3.1/src/core/stats.c
SOURCES += sources/network/lwip-1.3.1/src/core/sys.c
SOURCES += sources/network/lwip-1.3.1/src/core/tcp.c
SOURCES += sources/network/lwip-1.3.1/src/core/tcp_in.c
SOURCES += sources/network/lwip-1.3.1/src/core/tcp_out.c
SOURCES += sources/network/lwip-1.3.1/src/core/udp.c

SOURCES += sources/network/lwip-1.3.1/src/core/ipv4/autoip.c
SOURCES += sources/network/lwip-1.3.1/src/core/ipv4/icmp.c
SOURCES += sources/network/lwip-1.3.1/src/core/ipv4/igmp.c
SOURCES += sources/network/lwip-1.3.1/src/core/ipv4/inet.c
SOURCES += sources/network/lwip-1.3.1/src/core/ipv4/inet_chksum.c
SOURCES += sources/network/lwip-1.3.1/src/core/ipv4/ip_addr.c
SOURCES += sources/network/lwip-1.3.1/src/core/ipv4/ip.c
SOURCES += sources/network/lwip-1.3.1/src/core/ipv4/ip_frag.c

SOURCES += sources/network/lwip-1.3.1/src/core/snmp/asn1_dec.c
SOURCES += sources/network/lwip-1.3.1/src/core/snmp/asn1_enc.c
SOURCES += sources/network/lwip-1.3.1/src/core/snmp/mib2.c
SOURCES += sources/network/lwip-1.3.1/src/core/snmp/mib_structs.c
SOURCES += sources/network/lwip-1.3.1/src/core/snmp/msg_in.c
SOURCES += sources/network/lwip-1.3.1/src/core/snmp/msg_out.c

SOURCES += sources/network/lwip-1.3.1/src/netif/etharp.c
SOURCES += sources/network/lwip-1.3.1/src/netif/ethernetif.c
SOURCES += sources/network/lwip-1.3.1/src/netif/loopif.c
SOURCES += sources/network/lwip-1.3.1/src/netif/slipif.c

SOURCES += sources/network/lwip-1.3.1/port/ethernetif.c
SOURCES += sources/network/lwip-1.3.1/port/helloworld.c
#SOURCES += sources/network/lwip-1.3.1/port/httpd.c
#SOURCES += sources/network/lwip-1.3.1/port/fsdata.c
#SOURCES += sources/network/lwip-1.3.1/port/client.c
#SOURCES += sources/network/lwip-1.3.1/port/server.c
#SOURCES += sources/network/lwip-1.3.1/port/tftpserver.c
#SOURCES += sources/network/lwip-1.3.1/port/tftputils.c

