#include "Arduino.h"
#include "../io_cfg.h"
#include "../../../sys/sys_dbg.h"

void pinMode(uint8_t pin, uint8_t mode) {
	switch (pin) {

	case DS1302_CLK_PIN:
		if (mode == INPUT) {
			ds1302_clk_input_mode();
		}
		else if (mode == OUTPUT) {
			ds1302_clk_output_mode();
		}
		else if (mode == INPUT_PULLUP) {
			ds1302_clk_input_mode();
		}
		else {
			FATAL("AR", 0x01);
		}
		break;

	case DS1302_DATA_PIN:
		if (mode == INPUT) {
			ds1302_data_input_mode();
		}
		else if (mode == OUTPUT) {
			ds1302_data_output_mode();
		}
		else if (mode == INPUT_PULLUP) {
			ds1302_data_input_mode();
		}
		else {
			FATAL("AR", 0x01);
		}
		break;

	case DS1302_CE_PIN:
		if (mode == INPUT) {
			ds1302_ce_input_mode();
		}
		else if (mode == OUTPUT) {
			ds1302_ce_output_mode();
		}
		else if (mode == INPUT_PULLUP) {
			ds1302_ce_input_mode();
		}
		else {
			FATAL("AR", 0x01);
		}
		break;

	default:
		FATAL("AR", 0xF1);
		break;
	}
}

void digitalWrite(uint8_t pin, uint8_t val) {
	switch (pin) {

	case DS1302_CLK_PIN:
		if (val == HIGH) {
			ds1302_clk_digital_write_high();
		}
		else if (val == LOW) {
			ds1302_clk_digital_write_low();
		}
		else {
			FATAL("AR", 0x02);
		}
		break;

	case DS1302_DATA_PIN:
		if (val == HIGH) {
			ds1302_data_digital_write_high();
		}
		else if (val == LOW) {
			ds1302_data_digital_write_low();
		}
		else {
			FATAL("AR", 0x02);
		}
		break;

	case DS1302_CE_PIN:
		if (val == HIGH) {
			ds1302_ce_digital_write_high();
		}
		else if (val == LOW) {
			ds1302_ce_digital_write_low();
		}
		else {
			FATAL("AR", 0x02);
		}
		break;

	default:
		FATAL("AR", 0xF2);
		break;
	}
}

int digitalRead(uint8_t pin) {
	int val = 0;
	switch (pin) {

	case DS1302_DATA_PIN:
		val = ds1302_data_digital_read();
		break;

	default:
		FATAL("AR", 0xF3);
		break;
	}
	return val;
}
