#ifndef __TASK_LIST_H__
#define __TASK_LIST_H__

#include "../ak/ak.h"
#include "../ak/task.h"

extern task_t app_task_table[];

/*****************************************************************************/
/*  DECLARE: Internal Task ID
 *  Note: Task id MUST be increasing order.
 */
/*****************************************************************************/
/**
  * SYSTEM TASKS
  **************/
#define TASK_TIMER_TICK_ID				0

/**
  * APP TASKS
  **************/
#define AK_TASK_SHELL_ID				1
#define AK_TASK_LIFE_ID					2
#define AK_TASK_IF_ID					3
#define AK_TASK_FIRMWARE_ID				4
#define AK_TASK_LWIP_ID			        5
#define AK_TASK_MQTT_ID			        6

/**
  * EOT task ID
  **************/
#define AK_TASK_EOT_ID					7

/*****************************************************************************/
/*  DECLARE: Task entry point
 */
/*****************************************************************************/
extern void task_shell(ak_msg_t*);
extern void task_life(ak_msg_t*);
extern void task_if(ak_msg_t*);
extern void task_firmware(ak_msg_t*);
extern void task_lwip(ak_msg_t*);
extern void task_mqtt(ak_msg_t*);

#endif //__TASK_LIST_H__
