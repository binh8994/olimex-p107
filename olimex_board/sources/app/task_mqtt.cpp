#include <stdio.h>

#include "../ak/ak.h"
#include "../ak/timer.h"
#include "../ak/message.h"

#include "../app/app.h"
#include "../app/app_data.h"
#include "../app/app_dbg.h"

#include "../network/mqtt/mqtt.h"

#include "../common/utils.h"

#include "../lwipopts.h"
#include "../lwip/err.h"

#include "task_list.h"
#include "task_mqtt.h"

/* define SERVER*/
#define CUSTOM_MQTT_PORT				MQTT_PORT
#define KEEP_ALIVE						60
//uint8_t mqtt_sever_ip[4]   = {10, 42, 0, 1};
//uint8_t mqtt_sever_ip[4]   = {118, 69, 135, 199};
uint8_t mqtt_sever_ip[4]   = {37, 187, 106, 16};//"test.mosquitto.org"
/* topic */
const char * topic_to_pub = "iot/flights/data/demo/";
const char * topic_to_sub = "iot/flights/ctrl/demo/";
/* user */
const char* read_client_id = "iot-rclient";
const char* read_client_usr = "Z4BvdC";
const char* read_client_pw = "A3zw8zVTEpR6hZfH";

const char* read_write_client_id = "iot-rwclient";
const char* read_write_client_usr = "QET9pd";
const char* read_write_client_pw = "9zMRZyrPZtTEyCp3";
/* storage server ip */
static struct ip_addr server_ipaddr;

static mqtt_client_mng_t read_client_mng;
static mqtt_client_mng_t read_write_client_mng;

/* storage publish payload */
//char pub_payload[100] = {0};
/* callback function */
static void mqtt_read_client_connection_cb(mqtt_client_t *client, void *arg, mqtt_connection_status_t status);
static void mqtt_read_write_client_connection_cb(mqtt_client_t *client, void *arg, mqtt_connection_status_t status);
static void mqtt_sub_request_cb(void *arg, err_t result);
static void mqtt_incoming_publish_cb(void *arg, const char *topic, u32_t tot_len);
static void mqtt_incoming_data_cb(void *arg, const u8_t *data, u16_t len, u8_t flags);
static void mqtt_pub_request_cb(void *arg, err_t result);

void task_mqtt(ak_msg_t* msg){
	switch (msg->sig){
	case AK_MQTT_CONNECT_REQ:{
		APP_DBG("AK_MQTT_CONNECT_REQ\n");

		IP4_ADDR(&server_ipaddr, mqtt_sever_ip[0], mqtt_sever_ip[1], mqtt_sever_ip[2], mqtt_sever_ip[3]);

		mem_set(&read_client_mng, 0, sizeof(read_client_mng));
		read_client_mng.client_info.client_id = read_client_id;
		read_client_mng.client_info.client_user = read_client_usr;
		read_client_mng.client_info.client_pass = read_client_pw;
		read_client_mng.client_info.keep_alive = KEEP_ALIVE;
		read_client_mng.task_id = AK_TASK_MQTT_ID;
		read_client_mng.sig = AK_MQTT_READ_CLIENT_MQTT_CYCLYC;

		err_t err;
		err = mqtt_client_connect(&read_client_mng.mqtt_client, \
								  &server_ipaddr, \
								  CUSTOM_MQTT_PORT, \
								  mqtt_read_client_connection_cb, \
								  0, \
								  &read_client_mng.client_info);
		if(err != ERR_OK) {
			APP_DBG("mqtt_client_connect [read_mqtt_client]: return %d\n", err);
		}

		mem_set(&read_write_client_mng, 0, sizeof(read_write_client_mng));
		read_write_client_mng.client_info.client_id = read_write_client_id;
		read_write_client_mng.client_info.client_user = read_write_client_usr;
		read_write_client_mng.client_info.client_pass = read_write_client_pw;
		read_write_client_mng.client_info.keep_alive = KEEP_ALIVE;
		read_write_client_mng.task_id = AK_TASK_MQTT_ID;
		read_write_client_mng.sig = AK_MQTT_READ_WRITE_CLIENT_MQTT_CYCLYC;

		err = mqtt_client_connect(&read_write_client_mng.mqtt_client, \
								  &server_ipaddr, \
								  CUSTOM_MQTT_PORT, \
								  mqtt_read_write_client_connection_cb, \
								  0, \
								  &read_write_client_mng.client_info);
		if(err != ERR_OK) {
			APP_DBG("mqtt_client_connect [read_write_mqtt_client]: return %d\n", err);
		}
	}
		break;

	case AK_MQTT_READ_CLIENT_MQTT_CYCLYC : {
		//APP_DBG("AK_MQTT_READ_CLIENT_MQTT_CYCLYC\n");
		mqtt_cyclic_timer(&read_client_mng.mqtt_client);
	}
		break;

	case AK_MQTT_READ_WRITE_CLIENT_MQTT_CYCLYC : {
		//APP_DBG("AK_MQTT_READ_WRITE_CLIENT_MQTT_CYCLYC\n");
		mqtt_cyclic_timer(&read_write_client_mng.mqtt_client);
	}
		break;

	case AK_MQTT_INCOMING_PUBLISH:{
		APP_DBG("AK_MQTT_INCOMING_PUBLISH\n");

	}
		break;

	case AK_MQTT_OUTGOING_PUBLISH:{
		APP_DBG("AK_MQTT_OUTGOING_PUBLISH\n");

		const char *pub_payload= "Publish from gw";
		err_t err;
		err = mqtt_publish(&read_write_client_mng.mqtt_client, \
						   topic_to_pub, pub_payload, \
						   str_len((int8_t *)pub_payload), \
						   0, 0,
						   mqtt_pub_request_cb, \
						   0);
		if(err != ERR_OK) {
			APP_DBG("Publish err: %d\n", err);
		}

		//		uint8_t *buf = get_data_common_msg(msg);
		//		uint8_t data = *buf;

		//		mem_set(pub_payload, 0, 100);
		//		uint8_t len = sprintf(pub_payload, "{\"arrayOfLight\":[{\"id\":\"00000001\",\"current\":%d,\"status\":0}]}", data);
		//		pub_payload[len] = 0;

		//		err_t err;
		//		err = mqtt_publish(&read_write_client_mng.mqtt_client, topic_to_pub, pub_payload, str_len((int8_t *)pub_payload), 0, 0,
		//						   mqtt_pub_request_cb, 0);
		//		if(err != ERR_OK) {
		//			APP_DBG("Publish err: %d\n", err);
		//		}
	}
		break;

	default :
		break;
	}
}

static void mqtt_read_client_connection_cb(mqtt_client_t *client, void *arg, mqtt_connection_status_t status) {
	err_t err;
	mqtt_client_mng_t* client_mng = (mqtt_client_mng_t*)client;

	if(status == MQTT_CONNECT_ACCEPTED) {
		APP_DBG("mqtt_connection_cb [read_mqtt_client]: Successfully connected\n");

		/* Setup callback for incoming publish requests */
		mqtt_set_inpub_callback(&client_mng->mqtt_client, mqtt_incoming_publish_cb, mqtt_incoming_data_cb, arg);

		/* Subscribe to a topic */
		err = mqtt_subscribe(&client_mng->mqtt_client, topic_to_sub, 0, mqtt_sub_request_cb, arg);

		if(err != ERR_OK) {
			APP_DBG("mqtt_subscribe [read_mqtt_client]: return %d\n", err);
		}
	} else {
		APP_DBG("mqtt_connection_cb [read_mqtt_client]: Disconnected %d\n", status);

		/* Its more nice to be connected, so try to reconnect */
		err = mqtt_client_connect(&client_mng->mqtt_client, \
								  &server_ipaddr, \
								  CUSTOM_MQTT_PORT, \
								  mqtt_read_client_connection_cb, \
								  0, \
								  &client_mng->client_info);
		if(err != ERR_OK) {
			APP_DBG("mqtt_client_connect [read_mqtt_client]: return %d\n", err);
		}
	}
}


static void mqtt_read_write_client_connection_cb(mqtt_client_t *client, void *arg, mqtt_connection_status_t status) {
	err_t err;
	mqtt_client_mng_t* client_mng = (mqtt_client_mng_t*)client;

	if(status == MQTT_CONNECT_ACCEPTED) {
		APP_DBG("mqtt_connection_cb [read_write_mqtt_client]: Successfully connected\n");
	} else {
		APP_DBG("mqtt_connection_cb [read_write_mqtt_client]: Disconnected %d\n", status);

		/* Its more nice to be connected, so try to reconnect */
		err = mqtt_client_connect(&client_mng->mqtt_client, \
								  &server_ipaddr, \
								  CUSTOM_MQTT_PORT, \
								  mqtt_read_write_client_connection_cb, \
								  0, \
								  &client_mng->client_info);
		if(err != ERR_OK) {
			APP_DBG("mqtt_client_connect [read_write_mqtt_client]return %d\n", err);
		}
	}
}

static void mqtt_sub_request_cb(void *arg, err_t result) {
	/* Just print the result code here for simplicity,
	 normal behaviour would be to take some action if subscribe fails like
	 notifying user, retry subscribe or disconnect from server */
	if(result == ERR_OK){
		APP_DBG("mqtt_sub_request_cb: Subscribe success \n");
	}
	else {
		APP_DBG("mqtt_sub_request_cb: Subscribe error %d\n", result);
	}

}

static void mqtt_incoming_publish_cb(void *arg, const char *topic, u32_t tot_len) {
	APP_DBG("mqtt_incoming_publish_cb: topic [%s] length [%d]\n", topic, tot_len);
}

static void mqtt_incoming_data_cb(void *arg, const u8_t *data, u16_t len, u8_t flags) {
	if(flags & MQTT_DATA_FLAG_LAST) {
		/* Last fragment of payload received (or whole part if payload fits receive buffer
			See MQTT_VAR_HEADER_BUFFER_LEN)  */
		APP_DBG("mqtt_incoming_data_cb [unique_read_client]: Incoming data [%s]\n", data);
	} else {
		/* Handle fragmented payload, store in buffer, write to file or whatever */
	}
}

static void mqtt_pub_request_cb(void *arg, err_t result) {
	if(result == ERR_OK) {
		APP_DBG("mqtt_pub_request_cb: Publish success\n");
	}
	else {
		APP_DBG("mqtt_pub_request_cb: Publish error: %d\n", result);
	}
}
