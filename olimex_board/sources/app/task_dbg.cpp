#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"

#include "../sys/sys_ctrl.h"

#include "../common/utils.h"

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_dbg.h"

void task_dbg(ak_msg_t* msg) {
	switch (msg->sig) {
	case AC_DBG_TEST_1: {
		APP_DBG_SIG("AC_DBG_TEST_1\n");
		uint8_t ary_test_1[129];
		mem_set(ary_test_1, 0, 129);
		get_data_dynamic_msg(msg, ary_test_1, 129);
		for (int i = 0; i < 129; i++) {
			APP_DBG("%d: %d\n", i, ary_test_1[i]);
		}
	}
		break;

	default:
		break;
	}
}
