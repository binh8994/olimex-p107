/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

/* kernel include */
#include "../ak/ak.h"
#include "../ak/message.h"
#include "../ak/timer.h"

/* driver include */
#include "../driver/led/led.h"
#include "../driver/flash/flash.h"

/* app include */
#include "app.h"
#include "app_dbg.h"

/* task include */
#include "task_list.h"
#include "task_shell.h"
#include "task_life.h"
#include "task_if.h"

/* sys include */
#include "../sys/sys_irq.h"
#include "../sys/sys_io.h"
#include "../sys/sys_ctrl.h"
#include "../sys/sys_dbg.h"
#include "../sys/sys_arduino.h"

/* common include */
#include "../common/utils.h"

static void app_start_timer();

/*****************************************************************************/
/* app main function.
 */
/*****************************************************************************/
int main_app() {
	APP_PRINT("main_app() entry OK\n");

	/******************************************************************************
	* init active kernel
	*******************************************************************************/
	ENTRY_CRITICAL();
	task_init();
	task_create(app_task_table);
	EXIT_CRITICAL();

	/******************************************************************************
	* init applications
	*******************************************************************************/
	/*********************
	* hardware configure *
	**********************/

	/* init watch dog timer */
	sys_ctrl_independent_watchdog_init();	/* 32s */
	sys_ctrl_soft_watchdog_init(200);		/* 20s */

	/* ethernet io init */
	eth_io_init();
	/* ethernet init */
	eth_init();

	/*********************
	* software configure *
	**********************/
	/* life led init */
	led_init(&led_life, led_life_init, led_life_on, led_life_off);

	/* start timer for application */
	app_start_timer();

	ak_msg_t* smsg = get_pure_msg();
	set_msg_sig(smsg,AK_LWIP_INIT);
	task_post(AK_TASK_LWIP_ID, smsg);

	/******************************************************************************
	* run applications
	*******************************************************************************/
	return task_run();
}

/*****************************************************************************/
/* app initial function.
 */
/*****************************************************************************/

/* start software timer for application
 * used for app tasks
 */
void app_start_timer() {
	/* start timer to toggle life led */
	timer_set(AK_TASK_LIFE_ID, AK_LIFE_SYSTEM_CHECK, AK_LIFE_TASK_TIMER_LED_LIFE_INTERVAL, TIMER_PERIODIC);
}

