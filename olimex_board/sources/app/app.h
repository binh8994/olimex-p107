/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

#ifndef APP_H
#define APP_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "lwipopts.h"
#include "mqtt_opts.h"

#include "app_if.h"
#include "app_eeprom.h"

/*****************************************************************************/
/*  life task define
 */
/*****************************************************************************/
/* define timer */
#define AK_LIFE_TASK_TIMER_LED_LIFE_INTERVAL		(1000)

/* define signal */
#define AK_LIFE_SYSTEM_CHECK						(0)

/*****************************************************************************/
/*  shell task define
 */
/*****************************************************************************/
/* define timer */

/* define signal */
#define AK_SHELL_LOGIN_CMD							(0)
#define AK_SHELL_REMOTE_CMD							(1)

/*****************************************************************************/
/*  rf24 task define
 */
/*****************************************************************************/
/* define timer */
#define AK_RF24_IF_TIMER_PACKET_DELAY_INTERVAL		(100) /* 100 ms */

/* define signal */
/* interrupt signal */
#define AK_RF24_IF_IRQ_TX_FAIL						(1)
#define AK_RF24_IF_IRQ_TX_SUCCESS					(2)
#define AK_RF24_IF_IRQ_RX_READY						(3)

/* task signal */
#define AK_RF24_IF_INIT_NETWORK						(4)
#define AK_RF24_IF_PURE_MSG_OUT						(5)
#define AK_RF24_IF_COMMON_MSG_OUT					(6)
#define AK_RF24_IF_TIMER_PACKET_DELAY				(7)


/*****************************************************************************/
/*  warning sensor task define
 */
/*****************************************************************************/
/* define timer */
#define AK_SENSOR_TASK_COLLECT_WATER_DATA_INTERVAL         (150)
#define AK_SENSOR_TASK_COLLECT_STATUS_DETECT_INTERVAL      (1000)
/* define signal */
#define AK_SENSOR_DETECTOR_STATUS_REQ               (1)
#define AK_SENSOR_DETECTOR_STATUS_RES               (2)
#define AK_SENSOR_READ_DATA_REQ                     (3)
#define AK_SENSOR_READ_DATA_RES                     (4)
#define AK_SENSOR_GET_DATA_REQ                      (5)
#define AK_SENSOR_GET_DATA_RES                      (6)

/*****************************************************************************/
/* if task define
 */
/*****************************************************************************/
/* define timer */
#define AK_IF_TIMER_PACKET_TIMEOUT_INTERVAL			(500)

/* define signal */
#define AK_IF_PURE_MSG_IN							(1)
#define AK_IF_PURE_MSG_OUT							(2)
#define AK_IF_PURE_MSG_OUT_RES_OK					(3)
#define AK_IF_PURE_MSG_OUT_RES_NG					(4)
#define AK_IF_COMMON_MSG_IN							(5)
#define AK_IF_COMMON_MSG_OUT						(6)
#define AK_IF_COMMON_MSG_OUT_RES_OK					(7)
#define AK_IF_COMMON_MSG_OUT_RES_NG					(8)
#define AK_IF_PACKET_TIMEOUT						(9)

/*****************************************************************************/
/* firmware task define
 */
/*****************************************************************************/
/* define timer */
#define AK_TIMER_FIRMWARE_PACKED_TIMEOUT_INTERVAL	(2000)
#define AK_TIMER_FIRMWARE_CHECKING_INTERVAL			(1000)

/* define signal */
#define AK_FIRMWARE_CURRENT_INFO_REQ				(1)
#define AK_FIRMWARE_UPDATE_REQ						(2)
#define AK_FIRMWARE_UPDATE_SM_OK					(3)
#define AK_FIRMWARE_TRANSFER_REQ					(4)
#define AK_FIRMWARE_INTERNAL_UPDATE_RES_OK			(5)
#define AK_FIRMWARE_SAFE_MODE_RES_OK				(6)
#define AK_FIRMWARE_UPDATE_SM_BUSY					(7)
#define AK_FIRMWARE_PACKED_TIMEOUT					(8)
#define AK_FIRMWARE_CHECKING_REQ					(9)

/*****************************************************************************/
/*  global define variable
 */
/*****************************************************************************/
#define APP_OK                                      (0x00)
#define APP_NG                                      (0x01)

#define APP_FLAG_OFF                                (0x00)
#define APP_FLAG_ON                                 (0x01)

/*****************************************************************************/
/*  lwIP task define
 */
/*****************************************************************************/
/* define timer */
#define AK_LWIP_TCP_TMR_INTERVAL					(TCP_TMR_INTERVAL)
#define AK_LWIP_ARP_TMR_INTERVAL					(ARP_TMR_INTERVAL)
/* define signal */
#define AK_LWIP_INIT								(1)
#define AK_LWIP_TCP_TMR								(2)
#define AK_LWIP_ARP_TMR								(3)
#if LWIP_DHCP
/* define timer */
#define AK_LWIP_TASK_CHECK_GOT_IP_INTERVAL			(1000)
#define AK_LWIP_DHCP_FINE_INTERVAL					(DHCP_FINE_TIMER_MSECS)
#define AK_LWIP_DHCP_COARSE_INTERVAL				(DHCP_COARSE_TIMER_MSECS)
/* define signal */
#define AK_LWIP_CHECK_GOT_IP						(5)
#define AK_LWIP_GOT_IP_TIMEOUT						(6)
#define AK_LWIP_GOT_IP								(7)
#define AK_LWIP_DHCP_FINE							(8)
#define AK_LWIP_DHCP_COARSE							(9)
#endif

/*****************************************************************************/
/*  mqtt task define
 */
/*****************************************************************************/
/* define timer */
#define AK_MQTT_CYCLYC_INTERVAL						(MQTT_CYCLIC_TIMER_INTERVAL*1000)
/* define signal */
#define AK_MQTT_CONNECT_REQ							(1)
#define AK_MQTT_READ_CLIENT_MQTT_CYCLYC				(2)
#define AK_MQTT_READ_WRITE_CLIENT_MQTT_CYCLYC		(3)
#define AK_MQTT_INCOMING_PUBLISH					(4)
#define AK_MQTT_OUTGOING_PUBLISH					(5)
#define AK_MQTT_CLIENT_MQTT_CYCLYC					(6)

/*****************************************************************************/
/*  app function declare
 */
/*****************************************************************************/
#define AC_NUMBER_SAMPLE_CT_SENSOR				3000

extern int  main_app();

#ifdef __cplusplus
}
#endif

#endif //APP_H
