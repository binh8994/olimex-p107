#ifndef __APP_DATA_H__
#define __APP_DATA_H__
#include <stdint.h>
#include "app.h"
#include "mqtt.h"
/******************************************************************************
* Data type of RF24Network
*******************************************************************************/
#define RF24_DATA_COMMON_MSG_TYPE			(1)
#define RF24_DATA_PURE_MSG_TYPE				(2)
#define RF24_DATA_REMOTE_CMD_TYPE			(3)

/******************************************************************************
* Common define
*******************************************************************************/
#define IF_RETRY_COUNTER_MAX		3

/******************************************************************************
* Commom data structure for transceiver data
*******************************************************************************/
#define FIRMWARE_PSK		0x1A2B3C4D
#define FIRMWARE_LOK		0x1234ABCD

typedef struct {
	uint32_t psk;
	uint32_t bin_len;
	uint16_t checksum;
} firmware_header_t;

typedef struct {
	uint8_t smoke;
	uint8_t fire;
	uint8_t door;
	uint8_t button_fire_alarm;
	uint8_t water_sensor;
} warning_sensor_packet_t;

typedef union {
	struct {
		uint8_t sim_ip_addr_byte_1;
		uint8_t sim_ip_addr_byte_2;
		uint8_t sim_ip_addr_byte_3;
		uint8_t sim_ip_addr_byte_4;
	};
	uint32_t sim_ip_addr;
} simple_ip_addr_t;

typedef struct {
	mqtt_client_t mqtt_client;
	struct mqtt_connect_client_info_t client_info;
	uint8_t task_id;
	uint8_t sig;
} mqtt_client_mng_t;

#endif //__APP_DATA_H__
