#include "../ak/port.h"
#include "../ak/message.h"
#include "../ak/timer.h"

#include "../sys/sys_ctrl.h"
#include "../sys/sys_irq.h"
#include "../sys/sys_dbg.h"

#include "../common/utils.h"

#include "../platform/stm32f10x/Libraries/STM32_ETH_Driver/inc/stm32_eth.h"

#include "../lwipopts.h"
#include "../lwip/memp.h"
#include "../lwip/dhcp.h"
#include "../lwip/tcp.h"
#include "../lwip/udp.h"
#include "../netif/etharp.h"

#include "ethernetif.h"
#include "helloworld.h"

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_lwip.h"

#define MAX_DHCP_TRIES        4
#define CLIENTMAC6            2

struct netif netif;

uint8_t macaddress[6]={0,0,0,0,0,1};

uint8_t s_static_ip[4]   = {192, 168, 168,168};
uint8_t s_static_mask[4] = {255, 255, 255,0};
uint8_t s_static_gw[4]   = {192, 168, 168,1};

void task_lwip(ak_msg_t* msg) {

	switch (msg->sig) {
	case AK_LWIP_INIT:{
		APP_DBG("AK_LWIP_INIT\n");

		struct ip_addr ipaddr;
		struct ip_addr netmask;
		struct ip_addr gw;

		ipaddr.addr = 0;
		netmask.addr = 0;
		gw.addr = 0;

		/* Initializes the dynamic memory heap defined by MEM_SIZE.*/
		mem_init();
		/* Initializes the memory pools defined by MEMP_NUM_x.*/
		memp_init();

#if !LWIP_DHCP
		IP4_ADDR(&ipaddr, s_static_ip[0], s_static_ip[1], s_static_ip[2], s_static_ip[3]);
		IP4_ADDR(&netmask, s_static_mask[0], s_static_mask[1], s_static_mask[2], s_static_mask[3]);
		IP4_ADDR(&gw, s_static_gw[0], s_static_gw[1], s_static_gw[2], s_static_gw[3]);
#endif

		/* set MAC Address */
		Set_MAC_Address(macaddress);

		/* Adds your network interface to the netif_list. Allocate a struct
		netif and pass a pointer to this structure as the first argument.
		Give pointers to cleared ip_addr structures when using DHCP,
		or fill them with sane numbers otherwise. The state pointer may be NULL. */

		netif_add(&netif, &ipaddr, &netmask, &gw, NULL, &ethernetif_init, &ethernet_input);

		/* Registers the default network interface.*/
		netif_set_default(&netif);

#if LWIP_DHCP
		/* Creates a new DHCP client for this interface on the first call.
		Note: you must call dhcp_fine_tmr() and dhcp_coarse_tmr() at
		the predefined regular intervals after starting the client.
		You can peek in the netif->dhcp struct for the actual DHCP status.*/

		dhcp_start(&netif);
#endif
		/* When the netif is fully configured this function must be called.*/
		netif_set_up(&netif);

#if LWIP_DHCP
		timer_set(AK_TASK_LWIP_ID, AK_LWIP_CHECK_GOT_IP, AK_LWIP_TASK_CHECK_GOT_IP_INTERVAL, TIMER_PERIODIC);

		timer_set(AK_TASK_LWIP_ID, AK_LWIP_DHCP_FINE, AK_LWIP_DHCP_FINE_INTERVAL, TIMER_PERIODIC);
		timer_set(AK_TASK_LWIP_ID, AK_LWIP_DHCP_COARSE, AK_LWIP_DHCP_COARSE_INTERVAL, TIMER_PERIODIC);
#else
		simple_ip_addr_t ip_address;
		/* Read the new IP address */
		ip_address.sim_ip_addr = netif.ip_addr.addr;

		SYS_DBG( "Static IP: %d.%d.%d.%d\n", \
				 ip_address.sim_ip_addr_byte_1,\
				 ip_address.sim_ip_addr_byte_2,\
				 ip_address.sim_ip_addr_byte_3,\
				 ip_address.sim_ip_addr_byte_4);

#endif

		timer_set(AK_TASK_LWIP_ID, AK_LWIP_TCP_TMR, AK_LWIP_TCP_TMR_INTERVAL, TIMER_PERIODIC);
		timer_set(AK_TASK_LWIP_ID, AK_LWIP_ARP_TMR, AK_LWIP_ARP_TMR_INTERVAL, TIMER_PERIODIC);

	}
		break;

	case AK_LWIP_TCP_TMR :{
		tcp_tmr();
	}
		break;

	case AK_LWIP_ARP_TMR :{
		etharp_tmr();
	}
		break;

#if LWIP_DHCP

	case AK_LWIP_CHECK_GOT_IP : {
		APP_DBG("AK_LWIP_CHECK_GOT_IP\n");

		/* We have got a new IP address so update the display */
		if (netif.ip_addr.addr != (uint32_t) 0) {

			if (netif.flags & NETIF_FLAG_DHCP) {
				timer_remove_attr(AK_TASK_LWIP_ID, AK_LWIP_CHECK_GOT_IP);

				ak_msg_t *smsg = get_pure_msg();
				set_msg_sig(smsg, AK_LWIP_GOT_IP);
				task_post(AK_TASK_LWIP_ID, smsg);
			}

		}
		else {

			/* If no response from a DHCP server for MAX_DHCP_TRIES times */
			/* stop the dhcp client and set a static IP address */
			if (netif.dhcp->tries > MAX_DHCP_TRIES) {
				dhcp_stop(&netif);

				timer_remove_attr(AK_TASK_LWIP_ID, AK_LWIP_CHECK_GOT_IP);

				ak_msg_t *smsg = get_pure_msg();
				set_msg_sig(smsg, AK_LWIP_GOT_IP_TIMEOUT);
				task_post(AK_TASK_LWIP_ID, smsg);
			}
		}

	}
		break;

	case AK_LWIP_GOT_IP : {
		APP_DBG("AK_LWIP_GOT_IP\n");

		simple_ip_addr_t ip_address;
		/* Read the new IP address */
		ip_address.sim_ip_addr = netif.ip_addr.addr;

		SYS_DBG( "GOT IP: %d.%d.%d.%d\n", \
				 ip_address.sim_ip_addr_byte_1,\
				 ip_address.sim_ip_addr_byte_2,\
				 ip_address.sim_ip_addr_byte_3,\
				 ip_address.sim_ip_addr_byte_4);

		ak_msg_t *smsg = get_pure_msg();
		set_msg_sig(smsg, AK_MQTT_CONNECT_REQ);
		task_post(AK_TASK_MQTT_ID, smsg);

	}
		break;

	case AK_LWIP_GOT_IP_TIMEOUT : {
		APP_DBG("AK_LWIP_GOT_IP_TIMEOUT\n");

		struct ip_addr ipaddr;
		struct ip_addr netmask;
		struct ip_addr gw;

		IP4_ADDR(&ipaddr, s_static_ip[0], s_static_ip[1], s_static_ip[2], s_static_ip[3]);
		IP4_ADDR(&netmask, s_static_mask[0], s_static_mask[1], s_static_mask[2], s_static_mask[3]);
		IP4_ADDR(&gw, s_static_gw[0], s_static_gw[1], s_static_gw[2], s_static_gw[3]);

		/* set static ip addr */
		netif_set_addr(&netif, &ipaddr , &netmask, &gw);

		simple_ip_addr_t ip_address;
		/* Read the new IP address */
		ip_address.sim_ip_addr = netif.ip_addr.addr;

		SYS_DBG( "SET STATIC IP: %d.%d.%d.%d\n", \
				 ip_address.sim_ip_addr_byte_1,\
				 ip_address.sim_ip_addr_byte_2,\
				 ip_address.sim_ip_addr_byte_3,\
				 ip_address.sim_ip_addr_byte_4);

	}
		break;

	case AK_LWIP_DHCP_FINE :{
		dhcp_fine_tmr();
	}
		break;

	case AK_LWIP_DHCP_COARSE :{
		dhcp_coarse_tmr();
	}
		break;

#endif

	default:
		break;
	}

}

void eth_irq_handle(){
	/* Handles all the received frames */
	while(ETH_GetRxPktSize() != 0){
		/* Read a received packet from the Ethernet buffers and send it to the lwIP for handling */
		ethernetif_input(&netif);
	}
}
